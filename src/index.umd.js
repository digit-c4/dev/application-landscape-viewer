import React from 'react'
import ReactDOM from "react-dom/client";
import {Graph} from "./components";

import * as components from './components'

function ApplicationLandscape(args) {
    const {rootElementId = "root", props ={}} = args || {}
    const root = ReactDOM.createRoot(document.getElementById(rootElementId));
    const applicationLandscape = React.createElement(Graph, props);
    root.render(applicationLandscape);
}

ApplicationLandscape.components = components

export default ApplicationLandscape
