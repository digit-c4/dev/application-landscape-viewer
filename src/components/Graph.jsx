import React, { useRef, useEffect } from "react";
import PropTypes from "prop-types";
import * as d3 from "d3";
import "./graph.css";

export const Graph = ({ width, height, nodes, links }) => {
  const svgRef = useRef();

  useEffect(() => {
    const svg = d3
      .select(svgRef.current)
      .attr("width", width)
      .attr("height", height);

    let node = svg
      .selectAll(".node")
      .data(nodes)
      .enter()
      .append("circle")
      .attr("class", (d) => `node ${d.type}`)
      .attr("r", 30)
      .call(
        d3
          .drag()
          .on("start", dragstarted)
          .on("drag", dragged)
          .on("end", dragended)
      );

    svg
      .selectAll(".nodeText")
      .data(nodes)
      .enter()
      .append("text")
      .attr("class", "nodeText")
      .attr("dx", 7)
      .attr("dy", 0)
      .text((d) => d.name);

    const link = svg
      .selectAll(".link")
      .data(links)
      .enter()
      .append("line")
      .attr("class", "link")
      .style("stroke", "black")
      .style("stroke-width", 2)
      .lower();

    const linkText = svg
      .selectAll(".linkText")
      .data(links)
      .enter()
      .append("text")
      .attr("class", "linkText")
      .attr("dx", (d) => (d.source.x + d.target.x) / 2) // Positionnement horizontal au milieu du lien
      .attr("dy", (d) => (d.source.y + d.target.y) / 2) // Positionnement vertical au milieu du lien
      .text((d) => `${d.relation}`); // Texte à afficher

    // Fonctions de simulation du graphique
    const simulation = d3
      .forceSimulation(nodes)
      .force(
        "link",
        d3
          .forceLink(links)
          .id((d) => d.id)
          .distance(150)
      )
      .force("charge", d3.forceManyBody().strength(-100))
      .force("center", d3.forceCenter(width / 2, height / 2));

    simulation.on("tick", () => {
      link
        .attr("x1", (d) => d.source.x)
        .attr("y1", (d) => d.source.y)
        .attr("x2", (d) => d.target.x)
        .attr("y2", (d) => d.target.y);

      // Mettre à jour la position du texte pour chaque lien
      linkText
        .attr("dx", (d) => (d.source.x + d.target.x) / 2)
        .attr("dy", (d) => (d.source.y + d.target.y) / 2);

      node.attr("cx", (d) => d.x).attr("cy", (d) => d.y);

      // Positionnement des textes à côté des cercles
      svg
        .selectAll(".nodeText")
        .attr("x", (d) => d.x - 25)
        .attr("y", (d) => d.y + 5);
    });

    // Part to drag

    function dragstarted(event, d) {
      if (!event.active) simulation.alphaTarget(0.3).restart();
      d.fx = d.x;
      d.fy = d.y;
    }

    function dragged(event, d) {
      d.fx = event.x;
      d.fy = event.y;
    }

    function dragended(event, d) {
      if (!event.active) simulation.alphaTarget(0);
      d.fx = null;
      d.fy = null;
    }

    return () => {
      simulation.stop();
    };
  }, [width, height]);

  return <svg ref={svgRef}></svg>;
};

Graph.propTypes = {
  width: PropTypes.number,
  height: PropTypes.number,
  nodes: PropTypes.array,
  links: PropTypes.array,
};
Graph.defaultProps = {
  width: 800,
  height: 600,
  nodes: [],
  links: [],
};
