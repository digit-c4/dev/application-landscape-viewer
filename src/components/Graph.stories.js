import { Graph } from "./Graph";

export default {
  title: "Graph",
  component: Graph,
  tags: ["autodocs"],
  argTypes: {
    width: { control: "number" },
    height: { control: "number" },
    nodes: { control: "array" },
    links: { control: "array" },
  },
};

export const Default = {
  args: {
    width: 600,
    height: 400,
    nodes: [
      { id: "app_1", name: "app 1", type: "app" },
      { id: "app_2", name: "app 2", type: "app" },
      { id: "app_3", name: "app 3", type: "app" },
      { id: "api_1", name: "api 1", type: "api" },
      { id: "api_2", name: "api 2", type: "api" },
      { id: "api_3", name: "api 3", type: "api" },
      { id: "gui_1", name: "gui 1", type: "gui" },
      { id: "gui_2", name: "gui 2", type: "gui" },
      { id: "gui_3", name: "gui 3", type: "gui" },
    ],
    links: [
      { source: "app_1", target: "api_1", relation: "has as API" },
      { source: "app_1", target: "gui_1", relation: "has as GUI" },
      { source: "app_2", target: "api_2", relation: "has as API" },
      { source: "app_2", target: "gui_2", relation: "has as GUI" },
      { source: "app_3", target: "api_3", relation: "has as API" },
      { source: "app_3", target: "gui_3", relation: "has as GUI" },
      { source: "app_1", target: "api_2", relation: "has as API" },
    ],
  },
};
