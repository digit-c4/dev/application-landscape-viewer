# application-landscape-viewer

![wireframe.drawio.png](doc%2Fwireframe.drawio.png)

## Usage

### Web browser
> ⚠️ **WARNING** ⚠️
> 
> Gitlab generic registry will always return `application/octet-stream` content type. It is [by design](https://docs.gitlab.com/ee/user/packages/generic_packages/#download-package-file)
> and will most probably break the import in many browser. Chrome will indeed display fail
> 
> ```html
> Refused to execute script from 'https://code.europa.eu/api/v4/projects/digit-c4%2Fdev%2Fapplication-landscape-viewer
> /packages/generic/application-landscape-viewer/0.1.0/index.js' because its MIME type ('application/octet-stream') 
> is not executable, and strict MIME type checking is enabled.
> ```

```html
<html>
<head>
    <!-- Import react as peer dependency-->
    <script crossorigin src="https://unpkg.com/react@18.2.0/umd/react.development.js"></script>
    <script crossorigin src="https://unpkg.com/react-dom@18.2.0/umd/react-dom.development.js"></script>
    <script charset="UTF-8" type="text/javascript" src="https://code.europa.eu/api/v4/projects/digit-c4%2Fdev%2Fapplication-landscape-viewer/packages/generic/application-landscape-viewer/latest/index.js"></script>
    <link rel="stylesheet" type="text/css" href="https://code.europa.eu/api/v4/projects/digit-c4%2Fdev%2Fapplication-landscape-viewer/packages/generic/application-landscape-viewer/latest/styles.css">
</head>
<body>
<div id="root"></div>
<script>
  ApplicationLandscape({
    props: {
        nodes: [
            { id: "app_1", name: "app 1", type: "app" },
            { id: "app_2", name: "app 2", type: "app" },
            { id: "app_3", name: "app 3", type: "app" },
            ...
        ],
        links: [
            { source: "app_1", target: "api_1", relation: "has as API" },
            { source: "app_1", target: "gui_1", relation: "has as GUI" },
            ...
        ],
    }
  })
</script>
</body>
</html>

```

## Contributing
```shell
npm install
# Start and open Storybook
npm run dev
# Or start demo application
npm run demo
```

## Publish
The demonstration
```
npm run demo-build
```
or the library
```
npm run build
```
